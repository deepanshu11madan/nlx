apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "nlx-management.fullname" . }}-api
  labels:
    {{- include "nlx-management.labels" . | nindent 4 }}
    app.kubernetes.io/component: api
spec:
  replicas: {{ .Values.replicaCount }}
  selector:
    matchLabels:
      {{- include "nlx-management.selectorLabels" . | nindent 6 }}
      app.kubernetes.io/component: api
  template:
    metadata:
      labels:
        {{- include "nlx-management.selectorLabels" . | nindent 8 }}
        app.kubernetes.io/component: api
      annotations:
        checksum/configmap: {{ include (print $.Template.BasePath "/configmap.yaml") . | sha256sum }}
        checksum/secret: {{ include (print $.Template.BasePath "/secret.yaml") . | sha256sum }}
        checksum/secret-tls: {{ include (print $.Template.BasePath "/secret-tls.yaml") . | sha256sum }}
    spec:
    {{- with .Values.image.pullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
    {{- end }}
      serviceAccountName: {{ include "nlx-management.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        - name: nlx-management-api
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: {{ template "nlx-management.apiImage" . }}
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
            - name: http
              containerPort: 8080
              protocol: TCP
            - name: https
              containerPort: 8443
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /health
              port: http
          readinessProbe:
            httpGet:
              path: /health
              port: http
          env:
            - name: LISTEN_ADDRESS
              value: "0.0.0.0:8080"
            - name: CONFIG_LISTEN_ADDRESS
              value: "0.0.0.0:8443"
            - name: ETCD_CONNECTION_STRING
              value: http://{{ .Release.Name }}-etcd:2379
            - name: DIRECTORY_INSPECTION_ADDRESS
              value: {{ required "Directory inspection hostname is required" .Values.config.directoryInspectionHostname }}:443
            - name: DIRECTORY_REGISTRATION_ADDRESS
              value: {{ required "Directory registration hostname is required" .Values.config.directoryRegistrationHostname }}:443
            - name: SECRET_KEY
              valueFrom:
                secretKeyRef:
                  name: {{ template "nlx-management.fullname" . }}
                  key: oidc-session-sign-key
            - name: OIDC_CLIENT_ID
              value: {{ required "OIDC client ID is required" .Values.config.oidc.clientID }}
            - name: OIDC_CLIENT_SECRET
              value: {{ required "OIDC client secret is required" .Values.config.oidc.clientSecret }}
            - name: OIDC_DISCOVERY_URL
              value: {{ required "OIDC discovery URL is required" .Values.config.oidc.discoveryURL }}
            - name: OIDC_REDIRECT_URL
              value: {{ required "OIDC redirect URL is required" .Values.config.oidc.redirectURL }}
            - name: SESSION_COOKIE_SECURE
              value: {{ if .Values.config.sessionCookieSecure }}"1"{{ else }}"0"{{ end }}
            - name: TLS_NLX_ROOT_CERT
              value: /config/root-organization.pem
            - name: TLS_ORG_CERT
              value: /certificate-organization/tls.crt
            - name: TLS_ORG_KEY
              value: /certificate-organization/tls.key
            - name: TLS_ROOT_CERT
              value: /config/root.pem
            - name: TLS_CERT
              value: /certificate/tls.crt
            - name: TLS_KEY
              value: /certificate/tls.key
            - name: LOG_TYPE
              value: {{ .Values.config.logType }}
            - name: LOG_LEVEL
              value: {{ .Values.config.logLevel }}
          volumeMounts:
            - name: certificate-organization
              mountPath: /certificate-organization
              readOnly: true
            - name: certificate
              mountPath: /certificate
              readOnly: true
            - name: config
              mountPath: /config
              readOnly: true
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      volumes:
        - name: certificate-organization
          secret:
            secretName: {{ default (printf "%s-organization-tls" (include  "nlx-management.fullname" .)) .Values.tls.organizationCertificate.existingSecret }}
        - name: certificate
          secret:
            secretName: {{ default (printf "%s-tls" (include  "nlx-management.fullname" .)) .Values.tls.certificate.existingSecret }}
        - name: config
          configMap:
            name: {{ template "nlx-management.fullname" . }}
    {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
    {{- end }}
