// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package orgtls

import "go.nlx.io/nlx/common/cmd"

// TLSOptions defines the TLS options for a common NLX component.
type TLSOptions cmd.TLSOrgOptions
