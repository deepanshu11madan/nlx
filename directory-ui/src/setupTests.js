// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

configure({ adapter: new Adapter() })
