// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

export const AUTHORIZATION_TYPE_NONE = 'none'
export const AUTHORIZATION_TYPE_WHITELIST = 'whitelist'

export const AUTHORIZATION_TYPES = [
  AUTHORIZATION_TYPE_NONE,
  AUTHORIZATION_TYPE_WHITELIST,
]
