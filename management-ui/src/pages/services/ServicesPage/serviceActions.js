// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
export default {
  ADDED: 'added',
  EDITED: 'edited',
  REMOVED: 'removed',
}
