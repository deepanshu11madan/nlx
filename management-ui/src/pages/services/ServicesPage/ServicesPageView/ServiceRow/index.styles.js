// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'
import { Table } from '@commonground/design-system'

export const TdAlignRight = styled(Table.Td)`
  text-align: right;
`
